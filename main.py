# Définit un type de disposition
#   Exemples : Poste-Poste, poste-réseau, réseau-réseau
class Setup:
    name = ""
    nodes = []

# Définit un noeud sur le réseau
#   Exemples : Réseau (net), Passerelle (gate), Poste (end)
class Node:
    nodeType = ""
    addr_main = ""
    addr_sec = ""
    configs = []

# Définit un fichier de configuration
class Config:
    path = ""
    properties = {
    }

    def __init__(self, path):
        self.path = path
